import React from 'react';
import { connect } from 'react-redux';

import TabBar from 'components/TabBar';

import { goBack, closeModal, setStory } from 'store/router/actions';

import { Epic, View, Root, ConfigProvider } from '@vkontakte/vkui';

// Мэтчинг
import RatePanelBase from 'panels/rate/base';

// Экран мэтчей
import MatchesPanelBase from 'panels/matches/base';

// Экраны профиля
import HomePanelProfile from 'panels/profile/base';
import ProfilePanelAbout from 'panels/profile/about';

class App extends React.Component {
    lastAndroidBackAction = 0;

    componentDidMount() {
        const { goBack } = this.props;

        window.onpopstate = () => {
            let timeNow = +new Date();

            if (timeNow - this.lastAndroidBackAction > 500) {
                this.lastAndroidBackAction = timeNow;
                goBack('Android');
            } else {
                window.history.pushState(null, null);
            }
        };
    }

    componentDidUpdate(prevProps) {
        const { activeView, activeStory, activePanel, scrollPosition } = this.props;

        if (prevProps.activeView !== activeView || prevProps.activePanel !== activePanel || prevProps.activeStory !== activeStory) {
            let pageScrollPosition = scrollPosition[activeStory + '_' + activeView + '_' + activePanel] || 0;

            window.scroll(0, pageScrollPosition);
        }
    }

    render() {
        const { goBack, popouts, activeView, activeStory, activePanel, panelsHistory, colorScheme } = this.props;

        const history = panelsHistory[activeView] === undefined ? [activeView] : panelsHistory[activeView];
        const popout = popouts[activeView] === undefined ? null : popouts[activeView];

        const rootProps = {
            activeView,
            popout
        };

        const viewProps = {
            activePanel,
            history,
            onSwipeBack: () => goBack()
        };

        return (
            <ConfigProvider isWebView scheme={colorScheme}>
                <Epic activeStory={activeStory} tabbar={<TabBar />}>
                    <Root id="profile" {...rootProps}>
                        <View id="profile" {...viewProps}>
                            <HomePanelProfile id="base" />
                            <ProfilePanelAbout id="about" />
                        </View>
                    </Root>

                    <Root id="rate" {...rootProps}>
                        <View id="rate" header={false} {...viewProps}>
                            <RatePanelBase id="base" />
                        </View>
                    </Root>

                    <Root id="matches" {...rootProps}>
                        <View id="matches" {...viewProps}>
                            <MatchesPanelBase id="base" />
                        </View>
                    </Root>
                </Epic>
            </ConfigProvider>
        );
    }
}

const mapStateToProps = state => {
    return {
        activeView: state.router.activeView,
        activePanel: state.router.activePanel,
        activeStory: state.router.activeStory,
        panelsHistory: state.router.panelsHistory,
        activeModals: state.router.activeModals,
        popouts: state.router.popouts,
        scrollPosition: state.router.scrollPosition,

        colorScheme: state.vkui.colorScheme,
        epicShow: state.vkui.epicShow
    };
};

const mapDispatchToProps = {
    setStory,
    goBack,
    closeModal
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
