import React from 'react';

export default class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false, error: null };
    }

    componentDidCatch(error, errorInfo) {
        // this.setState({ error });

        this.setState({ hasError: true, error: { error: error, info: errorInfo } });
        // Display fallback UI
        // You can also log the error to an error reporting service
        // logErrorToMyService(error, info);
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <p style={{ width: '80%' }}>{JSON.stringify(this.state.error, null, 2)} Ошибка</p>;
        }
        return this.props.children;
    }
}
