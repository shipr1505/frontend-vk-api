import React, { Component, Suspense, lazy } from 'react';
import { connect } from 'react-redux';
import { ReactComponent as Logo } from 'img/logo.svg';

import { initApp } from 'services/VK';

const Progress = () => (
    <div className="preloader">
        <Logo className="reactPreloader" />
    </div>
);

class Preloader extends Component {
    state = { reg: '' };

    componentDidMount() {
        // Иницилизируем приложение
        const { dispatch } = this.props;
        dispatch(initApp());
    }

    render() {
        const { VKStorageLoaded, friendsPermission, isAuthorized, friendsLoading } = this.props;

        const Register = React.lazy(() => import('./Register'));
        return (
            <Suspense fallback={<Progress />}>
                <Register />
            </Suspense>
        );

        // Если мы ещё не подгрузили VKStorage
        if (!VKStorageLoaded || friendsLoading) return <Progress />;

        // Если пользователь в приложении впервые или нет доступа к друзьям
        if (!isAuthorized || !friendsPermission) {
            const Register = React.lazy(() => import('./Register'));
            return (
                <Suspense fallback={<Progress />}>
                    <Register step={isAuthorized ? 2 : 0} />
                </Suspense>
            );
        }

        // Если всё есть, всё прекрасно
        // Рендерим само приложение
        if (isAuthorized) {
            const App = React.lazy(() => import('./App'));
            return (
                <Suspense fallback={<Progress />}>
                    <App />
                </Suspense>
            );
        }

        return <Register />;
    }
}

const mapStateToProps = ({ serverData: { registered, VKStorageLoaded, friendsLoading }, vkui: { friendsPermission } }) => {
    return {
        friendsPermission,
        VKStorageLoaded,
        friendsLoading,
        isAuthorized: registered
    };
};

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Preloader);
