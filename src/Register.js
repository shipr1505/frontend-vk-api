import React from 'react';
import { connect } from 'react-redux';

import LayoutCover from 'img/layout.svg';
import 'css/register.css';
import { Button, classNames } from '@vkontakte/vkui';

import { setRegistered } from './store/serverData/actions';
import { friendsPermissionRequest } from 'services/VK';
import Icon56UsersOutline from '@vkontakte/icons/dist/56/users_outline';
import Icon28SettingsOutline from '@vkontakte/icons/dist/28/settings_outline';

class Register extends React.Component {
    state = {
        textNumber: null,
        show: false,
        dev: true
    };

    startAnimate = step => {
        if (step === 0) {
            setTimeout(() => this.setState({ textNumber: 0, show: true }), 100);
            setTimeout(() => this.setState({ show: false }), 5000);
            setTimeout(() => this.setState({ show: true, textNumber: 1 }), 6000);
            setTimeout(() => this.setState({ show: false }), 11000);
            setTimeout(() => this.setState({ show: true, textNumber: 2 }), 12000);
        } else setTimeout(() => this.setState({ show: true, textNumber: 2 }), 100);
    };

    allowFriends = () => {
        const { friendsPermissionRequest, setRegistered, step } = this.props;
        friendsPermissionRequest(step === 0);
        if (step === 0) setRegistered();
    };

    componentDidMount() {
        const { step } = this.props;
        this.startAnimate(step);
    }

    render() {
        const { show, textNumber, dev } = this.state;

        return (
            <div className="preloader" style={{ background: `white url(${LayoutCover})`, backgroundRepeat: 'no-repeat', backgroundSize: 'contain' }}>
                <div className="registerTextBox">
                    {dev ? (
                        <>
                            <Icon28SettingsOutline style={{ opacity: +show }} className="iconBig" />
                            <h5 style={{ opacity: +show }}>Мы в процессе разработки!</h5>
                            <p style={{ opacity: +show }}>
                                Дорогие пользователи! На данный момент, мы непрестанно трудимся для того, чтобы сделать наш сервис лучше.
                            </p>
                        </>
                    ) : (
                        <>
                            {textNumber === 2 && <Icon56UsersOutline style={{ opacity: +(textNumber === 2) }} />}
                            <h5 style={{ opacity: +show }}>
                                {textNumber === 0
                                    ? 'Узнай, с кем тебя шипперят!'
                                    : textNumber === 1
                                    ? 'Как это работает?'
                                    : textNumber === 2
                                    ? 'Ой, точно!'
                                    : null}
                            </h5>
                            <p style={{ opacity: +show }}>
                                {textNumber === 0 ? (
                                    'Более чем уверен, что не раз тебя интересовал вопрос, как считают твои друзья? Кто же из ваших общих знакомых больше тебе подходит?'
                                ) : textNumber === 1 ? (
                                    <>
                                        Это как Tinder, но наоборот. Не ты выбираешь себе пару, а твои друзья!
                                        <br /> <br /> <b>Ну что, начнём!</b>
                                    </>
                                ) : textNumber === 2 ? (
                                    'Чтобы приложение работало, нам нужен доступ к твоим друзьям!'
                                ) : null}
                            </p>
                            <Button
                                stretched
                                size="l"
                                className={classNames({
                                    'show-button': textNumber === 2
                                })}
                                onClick={this.allowFriends}
                            >
                                Предоставить доступ
                            </Button>
                        </>
                    )}
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
    friendsPermissionRequest,
    setRegistered
};

export default connect(
    null,
    mapDispatchToProps
)(Register);
