import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { goBack, closeModal, setStory } from '../store/router/actions';

import { Tabbar, TabbarItem, classNames } from '@vkontakte/vkui';

import Icon28Newsfeed from '@vkontakte/icons/dist/28/newsfeed';
import Icon28FireOutline from '@vkontakte/icons/dist/28/fire_outline';
import Icon28Profile from '@vkontakte/icons/dist/28/profile';

class TabBar extends React.Component {
    render() {
        const { setStory, activeStory, epicShow } = this.props;

        const itemClasses = id =>
            classNames({
                TabbarItem__hidden: !epicShow,
                [`TabbarItem__hidden_delay_${id}`]: true
            });

        const iconClasses = (currentStory, color) => ({
            transition: 'var(--icon-transition)',
            ...(activeStory === currentStory && { color: `var(--color-${color})`, transform: 'scale(1.2)' })
        });

        const TabbarStyle = () =>
            !epicShow
                ? {
                      transition: '0.3s ease-in-out',
                      opacity: 0
                  }
                : { background: 'blue' };

        return (
            <Tabbar style={TabbarStyle}>
                <TabbarItem onClick={() => setStory('profile', 'base')} selected={activeStory === 'profile'} className={itemClasses(1)}>
                    <Icon28Profile style={iconClasses('profile', 'blue')} />
                </TabbarItem>

                <TabbarItem onClick={() => setStory('rate', 'base')} selected={activeStory === 'rate'} className={itemClasses(2)}>
                    <Icon28Newsfeed style={iconClasses('rate', 'blue')} />
                </TabbarItem>
                <TabbarItem onClick={() => setStory('matches', 'base')} selected={activeStory === 'matches'} className={itemClasses(3)}>
                    <Icon28FireOutline style={iconClasses('matches', 'orange')} />
                </TabbarItem>
            </Tabbar>
        );
    }
}

const mapStateToProps = state => {
    return {
        activeStory: state.router.activeStory,
        epicShow: state.vkui.epicShow
    };
};

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        ...bindActionCreators({ setStory, goBack, closeModal }, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TabBar);
