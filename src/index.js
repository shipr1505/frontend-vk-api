import 'core-js/es6/map';
import 'core-js/es6/set';

import React from 'react';
import ReactDOM from 'react-dom';
import Preloader from './Preloader';
// Redux setup
import { Provider } from 'react-redux';

// Styles
import '@vkontakte/vkui/dist/vkui.css';
import './css/main.css';
import './css/fonts.css';

import { configureStore } from 'store';
import ErrorBoundary from './ErrorBoundary';

// Servise-worker
import register from './sw';

// Store init
export const store = configureStore();

ReactDOM.render(
    <ErrorBoundary>
        <Provider store={store}>
            <Preloader />
        </Provider>
    </ErrorBoundary>,
    document.getElementById('root')
);

register();
