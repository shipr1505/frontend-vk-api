import React from 'react';
import { connect } from 'react-redux';

import { setPage } from 'store/router/actions';
import { shareToWall } from 'services/VK';
import Icon56FireOutline from '@vkontakte/icons/dist/56/fire_outline';
import { Panel, PanelHeader, Placeholder, Button, PullToRefresh, Group, List, Cell, Avatar, PanelSpinner, Link } from '@vkontakte/vkui';
import 'css/matches.css';
import API from 'services/api';

class HomePanelProfile extends React.Component {
    state = {
        fetching: false,
        loaded: true,
        matches: null
        // [{name: 'Артём', surname: 'Штельцер', score: '98',
        // photo_link: 'https://sun9-47.userapi.com/c855524/v855524902/116768/0M1rZi5vvIc.jpg'}]
    };

    API = new API();

    refresh = () => {
        this.setState({ fetching: true });
        return this.API.getMatches().then(({ matches }) => {
            this.setState(prev => ({ ...prev, fetching: false, matches: matches }));
            return true;
        });
    };

    componentDidMount() {
        this.refresh().then(() => this.setState({ loaded: false }));
    }

    getAge = seconds => {
        return ~~(seconds / 3600 / 24 / 365);
    };

    render() {
        const { id } = this.props;
        const { fetching, matches, loaded } = this.state;

        return (
            <Panel id={id}>
                <PanelHeader noShadow>
                    <span style={{ color: 'var(--color-orange)' }}>Мои мэтчи</span>
                </PanelHeader>
                <PullToRefresh onRefresh={this.refresh} isFetching={fetching}>
                    {loaded ? (
                        <PanelSpinner />
                    ) : matches ? (
                        <Group>
                            <List>
                                {matches.map(({ user: { id, photo_link, birth_date: { seconds }, name, surname }, score }) => (
                                    <Link key={id} className="cell-wrapper" href={`https://vk.com/id${id}`} target="_blank">
                                        <Cell
                                            size="l"
                                            onClick={() => {}}
                                            before={<Avatar size={72} src={photo_link} />}
                                            description={`${this.getAge(seconds)} годиков`}
                                            asideContent={<div className="score">{parseInt(score * 100)}</div>}
                                        >
                                            {`${name} ${surname}`}
                                        </Cell>
                                    </Link>
                                ))}
                            </List>
                        </Group>
                    ) : (
                        <div style={{ height: 'calc(100vh - 120px)', width: '100%', display: 'flex', alignContent: 'center', justifyContent: 'center' }}>
                            <Placeholder
                                icon={<Icon56FireOutline style={{ color: 'var(--color-orange)' }} />}
                                header="У тебя ещё нет мэтчей"
                                action={
                                    <Button stretched size="l" className="orange-background" onClick={shareToWall}>
                                        Поделиться
                                    </Button>
                                }
                            >
                                Поделись приложением, чтобы друзья нашли тебе пару
                            </Placeholder>
                        </div>
                    )}
                </PullToRefresh>
            </Panel>
        );
    }
}

const mapStateToProps = state => {
    return {
        activeTab: state.vkui.activeTab
    };
};

const mapDispatchToProps = {
    setPage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePanelProfile);
