import React from 'react';
import { connect } from 'react-redux';

import { goBack } from 'store/router/actions';

import { Panel, Div, PanelHeader, PanelHeaderBack } from '@vkontakte/vkui';

class ProfilePanelAbout extends React.Component {
    render() {
        const { id, goBack } = this.props;

        return (
            <Panel id={id}>
                <PanelHeader left={<PanelHeaderBack onClick={() => goBack()} />}>О нас</PanelHeader>
                <Div>
                    <h3 className="titleAbout">
                        ЧЁ ЭТО ТАКОЕ <span>SHIPPR</span>?
                    </h3>
                    <p className="descriptionAbout">
                        Как ты думаешь, кто лучше всех подойдёт твоему другу? Попробуй подобрать ему лучшую пару из своих знакомых. SHIPPR, по-сути, Tinder
                        наоборот. Ты ищешь пару друзьям - друзья ищут пару тебе.
                    </p>
                    <h3 className="titleAbout">КТО ВЫ ТАКИЕ?</h3>
                    <p className="descriptionAbout">
                        <span>SHIPPR</span> — это оупен-сорс проект, поэтому ты можешь делать что хочешь: форкай, коммить, изменяй проект, — без каких-либо на
                        то запретов. Чекай наш{' '}
                        <a href="https://gitlab.com/shipr1505" target="_blank">
                            GitLab
                        </a>
                        !
                    </p>
                    <h3 className="titleAbout">МЫ ГДЕ-ТО НАКОСЯЧИЛИ?</h3>
                    <p className="descriptionAbout">
                        Нашёлся баг? Зарепорть его в{' '}
                        <a href="https://vk.com/shipper_app" target="_blank">
                            нашем сообществе
                        </a>{' '}
                        или напиши Мазуру.
                    </p>
                </Div>
            </Panel>
        );
    }
}

const mapDispatchToProps = {
    goBack
};

export default connect(
    null,
    mapDispatchToProps
)(ProfilePanelAbout);
