import React from 'react';
import { connect } from 'react-redux';
import { setPage } from 'store/router/actions';
import { setProfileData } from 'store/userInfo/actions';

import { Div, Input, FormLayout, FormLayoutGroup, Panel, Group, PanelHeader, Avatar, CellButton, Textarea, classNames, RangeSlider } from '@vkontakte/vkui';
import Icon28InfoOutline from '@vkontakte/icons/dist/28/info_outline';

import ContentLoader from 'react-content-loader';
import 'css/profile.css';
import API from 'services/api';

const months = {
    1: 'Января',
    2: 'Февраля',
    3: 'Марта',
    4: 'Апреля',
    5: 'Мая',
    6: 'Июня',
    7: 'Июля',
    8: 'Августа',
    9: 'Сентября',
    10: 'Октября',
    11: 'Ноября',
    12: 'Декабря'
};

class HomePanelProfile extends React.Component {
    state = {
        inputData: {
            preferSex: null,
            preferAge: [18, 24],
            age: 16
        },
        userInfo: {
            name: 'Арсений Чеканов',
            userAge: '17 годиков',
            picture_href: null
        },
        loaded: false
    };

    API = new API();
    timeout = null;

    getDateArray = bdate => {
        return;
    };

    getAge = bdate => {
        if (!bdate) return 'Возраст скрыт';
        bdate = bdate
            .split('.')
            .map(val => (val.length < 2 ? `0${val}` : val))
            .reverse();

        if (bdate.length) {
            const newMoment = new Date(bdate.join('-'));
            bdate = `${parseInt(newMoment.getDate())} ${months[newMoment.getMonth() + 1]}`;
        }

        return bdate;
    };

    componentDidMount() {
        const {
            userInfo: { first_name, last_name, photo_200, bdate },
            profileData: {
                birth_date,
                preferred_age: { min_age, max_age },
                preferred_gender
            }
        } = this.props;

        this.setState(prev => ({
            ...prev,
            loaded: true,
            inputData: {
                preferAge: [min_age, max_age],
                preferSex: +preferred_gender,
                age: birth_date
            },
            userInfo: {
                name: `${first_name} ${last_name}`,
                picture_href: photo_200,
                userAge: bdate && this.getAge(bdate)
            }
        }));
    }

    updateData = () => {
        const { setProfileData } = this.props;
        const { inputData } = this.state;
        const newData = {
            preferred_gender: !!inputData.preferSex,
            preferred_age: { min_age: inputData.preferAge[0], max_age: inputData.preferAge[1] },
            birth_date: inputData.age
        };
        setProfileData(newData);
        this.API.updateProfile(newData);
    };

    handleInner = (value, name) => {
        this.setState(previousState => ({ ...previousState, inputData: { ...previousState.inputData, [name]: value } }));
    };

    handleChange = (e, newValue) => {
        const { value, name } = e.target;
        let { inputData } = this.state;
        inputData[name] = newValue !== undefined ? newValue : value;
        this.setState({ inputData });
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => this.updateData(), 2000);
    };

    componentWillUnmount() {
        clearTimeout(this.timeout);
        this.updateData();
    }

    render() {
        const { id, setPage } = this.props;
        const {
            userInfo: { name, userAge },
            inputData: { age, preferSex, preferAge },
            userInfo: { picture_href },
            loaded
        } = this.state;

        return (
            <Panel id={id}>
                <PanelHeader noShadow>
                    <span style={{ color: 'var(--color-blue)' }}>Мой профиль</span>
                </PanelHeader>
                <Group>
                    <div style={{ display: 'flex', width: '100%', flexDirection: 'row', alignItems: 'center', margin: '1rem 0', padding: '0 12px' }}>
                        {loaded ? (
                            <>
                                <Avatar src={picture_href} size={100} style={{ height: 100, width: 100 }} />
                                <Div className="profileWrapper">
                                    <p className="name-title">{name}</p>
                                    <p className="name-title name-secondary">{userAge}</p>
                                </Div>
                            </>
                        ) : (
                            <>
                                <ContentLoader viewBox="0 0 500 100" style={{ height: 100, width: 500 }}>
                                    <rect x="0" y="0" rx="100" ry="100" width="100" height="100" />
                                    <rect x="138" y="25" rx="10" ry="10" width="172" height="24" />
                                    <rect x="138" y="55" rx="10" ry="10" width="110" height="21" />
                                </ContentLoader>
                            </>
                        )}
                    </div>
                    <FormLayout>
                        <FormLayoutGroup top="ВОЗРАСТ">
                            <Input value={age} onChange={this.handleChange} name="age" placeholder="Ваш возраст" type="number" />
                        </FormLayoutGroup>
                        <FormLayoutGroup top="КОГО ВЫ ПРЕДПОЧИТАЕТЕ?">
                            <div className="selectsWrapper">
                                <button
                                    name="preferSex"
                                    onClick={e => this.handleChange(e, 0)}
                                    className={classNames({
                                        active: preferSex === 0,
                                        selectButton: true
                                    })}
                                >
                                    Мужчины
                                </button>
                                <button
                                    name="preferSex"
                                    onClick={e => this.handleChange(e, 1)}
                                    className={classNames({
                                        active: preferSex === 1,
                                        selectButton: true
                                    })}
                                >
                                    Женщины
                                </button>
                                {/* <button
                                    name="preferSex"
                                    onClick={e => this.handleChange(e, 2)}
                                    className={classNames({
                                        active: preferSex === 2,
                                        selectButton: true
                                    })}
                                >
                                    Не важно
                                </button> */}
                            </div>
                        </FormLayoutGroup>
                        <FormLayoutGroup top="ВОЗРАСТНОЙ ДИАПАЗОН" style={{ position: 'relative' }}>
                            <label
                                htmlFor="preferAge"
                                style={{
                                    position: 'absolute',
                                    top: -24,
                                    right: 12,
                                    fontWeight: 600
                                }}
                            >
                                от {preferAge[0]} до {preferAge[1] === 55 ? '55+' : preferAge[1]}
                            </label>
                            <RangeSlider
                                name="preferAge"
                                top="Выберите возраст"
                                min={12}
                                max={55}
                                step={1}
                                defaultValue={[18, 24]}
                                value={preferAge}
                                onChange={value => this.handleInner(value, 'preferAge')}
                            />
                        </FormLayoutGroup>
                        <Div top="ДОПОЛНИТЕЛЬНОЕ" style={{ padding: '1rem 0' }}>
                            <CellButton onClick={() => setPage('profile', 'about')} before={<Icon28InfoOutline />}>
                                О нас
                            </CellButton>
                        </Div>
                    </FormLayout>
                </Group>
            </Panel>
        );
    }
}

const mapStateToProps = ({ userInfo: { userInfo, profileData } }) => ({
    userInfo,
    profileData
});

const mapDispatchToProps = {
    setPage,
    setProfileData
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePanelProfile);
