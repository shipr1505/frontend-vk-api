import React from 'react';
import { connect } from 'react-redux';

import { setEpicShow } from 'store/vk/actions';

import StoryOutline from '@vkontakte/icons/dist/28/story_outline';
import API from 'services/api';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import 'css/rangeslider.css';
import RateOptions from 'components/RateOptions';
import { getUserFriends } from 'services/VK';

class HomePanelBase extends React.Component {
    state = {
        newPair: null,
        pair: null,

        rate: 0,
        finished: false,
        rating: [],
        selectedUser: 0,
        fetching: false
    };

    API = new API();

    loadNew = () => {
        const { setEpicShow } = this.props;
        setEpicShow(true);
        this.setState(prev => ({ ...prev, rate: 0, finished: false, pair: prev.newPair }));
    };

    componentDidMount() {
        this.loadNew();
        this.API.getSamplePair().then(result => this.setState({ pair: result }));
    }

    startedRate = () => {
        const { setEpicShow } = this.props;
        setEpicShow(false);
        this.setState({ finished: false });
        this.API.getSamplePair().then(result => {
            this.setState({ newPair: result });

            result.users.forEach(({ photo_link }) => {
                const img = new Image();
                img.src = photo_link;
            });
        });
    };

    finishedRate = value => {
        const { pair_id } = this.state.pair;
        this.setState({ fetching: true });
        this.API.ratePair({ pair_id, rating: value }).then(({ ratings }) => {
            this.setState(prev => ({ ...prev, rating: ratings, finished: true, fetching: false }));
        });
    };

    render() {
        const { rate, finished, rating, pair, selectedUser, fetching } = this.state;
        return (
            <div className="photoWrapper">
                <div className={['photoBox', ...[finished ? 'active' : null]].join(' ')}>
                    <div
                        className="photoRound photoRound-1"
                        onClick={() => this.setState({ selectedUser: 0 })}
                        style={pair && pair.users && { backgroundImage: `url(${pair.users[0].photo_link})` }}
                    />
                    <div
                        className="photoRound photoRound-2"
                        onClick={() => this.setState({ selectedUser: 1 })}
                        style={pair && pair.users && { backgroundImage: `url(${pair.users[1].photo_link})` }}
                    />
                    <div className="photoRound photoRound-3 men-gradient">
                        <p>
                            {pair && pair.users && pair.users[selectedUser].name}
                            <br />
                            {pair && pair.users && pair.users[selectedUser].surname}
                        </p>
                    </div>
                </div>
                <div className="rateBox">
                    <p>
                        {finished ? (
                            <>
                                Шипперишь?
                                <br />
                                Ну ладно, вот как думают
                                <br /> об этой парочке другие.
                            </>
                        ) : (
                            'Как вы оцените эту пару?'
                        )}
                    </p>
                    <div className="sliderWrapper">
                        {!finished && (
                            <InputRange
                                minValue={0}
                                maxValue={10}
                                value={rate}
                                onChangeStart={this.startedRate}
                                onChange={rate => this.setState({ rate })}
                                onChangeComplete={this.finishedRate}
                                disabled={fetching}
                            />
                        )}
                        {finished && <RateOptions data={rating} />}
                    </div>
                    {finished && (
                        <div className="actionsWrapper">
                            {/* <StoryOutline style={{ color: 'var(--color-orange)', transform: 'scale(1.3)' }} onClick={this.shareHistory} /> */}
                            <button onClick={this.loadNew} className="actionsButton">
                                ПРОДОЛЖИТЬ
                            </button>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ vkui: { epicShow } }) => ({ epicShow });
const mapDispatchToProps = { setEpicShow };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePanelBase);
