import bridge from '@vkontakte/vk-bridge';
import { setAccessToken, setQueryData, setColorScheme } from 'store/vk/actions';
import { setInitialData, setFriendsLoading, setPrivateProfile } from 'store/serverData/actions';
import { setUserInfo, setProfileData } from 'store/userInfo/actions';
import API from 'services/api';
import { store } from 'index';

const APP_ID = 7289725;
const API_VERSION = 5.103;

export const initApp = () => dispatch => {
    const VKConnectOldCallback = e => {
        if (e.detail.type === 'VKWebAppUpdateConfig') dispatch(setColorScheme(e.detail.data.scheme));
    };

    bridge.subscribe(VKConnectOldCallback);
    bridge.send('VKWebAppInit', {});

    // Получаем query параметры и смотрим, есть ли доступ к друзьям
    // dispatch(getQueryParams());
    // dispatch(getVKStorageData());
};

export const getQueryParams = () => dispatch => {
    // Получаем query params, передаваемые VK и парсим их
    const queryString = document.location.search.slice(1).split('&');
    let queryParams = {};
    for (let item of queryString) {
        let pair = item.split('=');
        queryParams[pair[0]] = pair[1];
    }

    console.log('QueryParams', queryParams);
    // Сохраняем query параметры в стор
    dispatch(setQueryData(queryParams, queryString));

    // Чекаем, есть ли доступ к друзьям
    // Если он есть -- обновляем Access Token на всякий случай
    if (queryParams.vk_access_token_settings && queryParams.vk_access_token_settings.includes('friends')) dispatch(friendsPermissionRequest());
};

export const closeApp = () => {
    bridge.send('VKWebAppClose', { status: 'success' });
};

export const friendsPermissionRequest = (loadFriends = false) => dispatch => {
    return bridge
        .send('VKWebAppGetAuthToken', {
            app_id: APP_ID,
            scope: 'friends,status'
        })
        .then(data => {
            console.log(data.access_token);
            dispatch(setAccessToken(data.access_token));
            if (store.getState().serverData.privateAccount && loadFriends) dispatch(getUserFriends(data.access_token));
        })
        .catch(err => {
            console.groupCollapsed('FriendsPermission Request Error');
            console.log(`Ошибка: ${err.error_data.error_reason}`);
            console.groupEnd();
        });
};

export const getVKStorageData = () => dispatch => {
    return bridge
        .send('VKWebAppStorageGetKeys', { count: 2, offset: 0 })
        .then(res => {
            console.groupCollapsed('VKSTORAGE');
            console.log('keys', res);

            // TODO: Убрать отрицание, сейчас оно нужно, чтобы обновлялся этот долбанный докен
            const isRegistered = res.keys.includes('auth');

            if (!isRegistered) {
                // Получаем токен авторизации серверных запросов
                // Устанавливаем их в VK Stoarage
                // Получаем данные о пользователе и сразу апдейтим профиль в бд на бэке
                new API().auth().then(({ access_token, private_account }) => {
                    dispatch(setPrivateProfile(private_account));
                    dispatch(setStorageValues(access_token));
                    console.groupEnd();
                });
            } else {
                new API().auth().then(({ access_token }) => {
                    bridge.send('VKWebAppStorageSet', { key: 'auth', value: access_token }).then(res => {
                        // Получаем токен из хранилища
                        // Обновляем данные профиля
                        dispatch(getStorageValues());
                    });
                });
            }
        })
        .catch(err => console.log(err));
};

export const getStorageValues = () => dispatch =>
    bridge.send('VKWebAppStorageGet', { keys: ['auth'] }).then(({ keys }) => {
        const token = keys[0].value;

        // Устанавливаем JWTToken и говорим, что пользователь уже был зареган
        dispatch(
            setInitialData({
                jwtToken: token,
                registered: true
            })
        );

        // Получаем актуальную информацию о пользователе из VK и сохраняем в стор
        dispatch(userInfoGet());

        // Получаем данные о предпочтениях с сервака
        new API(token).getProfile().then(res => {
            console.log('GET PROFILE', res);
            // Устанавливаем их в стор
            dispatch(setProfileData(res));
        });

        console.groupEnd();
    });

export const setStorageValues = token => dispatch => {
    // Получаем информацию о пользователе от VK и сеттим её в стор
    dispatch(userInfoGet()).then(res => {
        // Получаем обработанную информацию
        // для оптимальной работы с бэком
        // и устанавливаем её в стор
        dispatch(setProfileData(res));

        // Изменяем объект профиля пользователя на серваке
        new API(token).updateProfile(res);

        // Сетапим для продолжения работы
        dispatch(
            setInitialData({
                jwtToken: token,
                registered: false
            })
        );
    });
    // Сохраняем jwtToken в VKStorage
    bridge.send('VKWebAppStorageSet', { key: 'auth', value: token });
};

export const userInfoGet = () => dispatch =>
    bridge.send('VKWebAppGetUserInfo').then(res => {
        const { sex, bdate } = res;

        // Получив данные пользователя, устанавливаем его возраст и предпочтения
        // Если дата рождения не открыта, то ставим возраст 16, по умолчанию
        let birth_date = 16;

        if (bdate) {
            const parsed_date = bdate
                .split('.')
                .map(val => (val.length < 2 ? `0${val}` : val))
                .reverse();
            if (parsed_date.length !== 2) birth_date = new Date().getFullYear() - new Date(parsed_date).getFullYear();
        }

        const userProfile = {
            preferred_age: { min_age: 16, max_age: 25 },
            preferred_gender: sex === 2 ? true : false,
            birth_date
        };

        // Устанавливаем данные с ВК в стор
        dispatch(setUserInfo(res));
        return userProfile;
    });

export const shareToWall = () => bridge.send('VKWebAppShare', { link: 'https://vk.com/app7289725' });

export const APICall = (method, params = {}, access_token) => {
    params['access_token'] = access_token || store.getState().vkui.accessToken;
    params['v'] = params['v'] === undefined ? API_VERSION : params['v'];

    return bridge
        .send('VKWebAppCallAPIMethod', {
            method,
            params
        })
        .then(data => {
            return data;
        })
        .catch(error => error);
};

export const getUserFriends = access_token => dispatch => {
    dispatch(setFriendsLoading(true));
    APICall('friends.get', {
        order: 'hints',
        fields: 'nickname, domain, sex, bdate, photo_200_orig',
        access_token
    }).then(res => {
        console.log('GOT FRIENDS', res, access_token);
        if (res && res.response && res.response.items) {
            const newItems = res.response.items.map(item => ({
                id: item.id,
                gender: item.sex,
                name: item.first_name,
                surname: item.last_name,
                photo_url: item.photo_200_orig
            }));
            new API().updateFriends(newItems).then(() => {
                dispatch(setFriendsLoading(false));
            });
        }
    });
};

// export const createStory = data => {
//     bridge.send('VKWebAppShowStoryBox', {
//         locaked: true,
//         background_type: 'image',
//         blob: 'https://sun9-65.userapi.com/c850136/v850136098/1b77eb/0YK6suXkY24.jpg'
//     });
// };
