import { store } from 'index';

const toSeconds = age => ({ seconds: Math.floor(new Date().getTime() / 1000 - age * (60 * 60 * 24 * 365)) });
const toAge = ({ seconds }) => Math.floor((new Date().getTime() / 1000 - seconds) / (60 * 60 * 24 * 365));

class API {
    constructor(token) {
        this.prod = false;
        this.host = this.prod ? '' : 'https://shipper.fun';
        this.token = token || store.getState().serverData.jwtToken;
        this.headers = {
            Authorization: `Bearer ${this.token}`,
            'Content-Type': 'application/json'
        };
    }

    auth = () =>
        fetch(`${this.host}/users/auth`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ url: window.location.href })
        })
            .then(response => response.json())
            .catch(err => console.log(err));

    getSamplePair = () =>
        fetch(`${this.host}/matchmaking/sample_pair`, {
            method: 'POST',
            headers: this.headers,
            body: '{}'
        }).then(rep => rep.json());

    ratePair = data =>
        fetch(`${this.host}/matchmaking/rate_pair`, {
            method: 'POST',
            headers: this.headers,
            body: JSON.stringify(data)
        }).then(rep => rep.json());

    getMatches = () =>
        fetch(`${this.host}/matchmaking/matches`, {
            method: 'POST',
            headers: this.headers,
            body: '{}'
        }).then(rep => rep.json());

    getProfile = () =>
        fetch(`${this.host}/users/get_profile`, {
            method: 'POST',
            headers: this.headers,
            body: '{}'
        })
            .then(rep => rep.json())
            .then(({ profile }) => {
                console.log(profile);
                let newProfile = {
                    ...profile,
                    birth_date: toAge(profile.birth_date),
                    preferred_age: { min_age: toAge(profile.preferred_age.min_age), max_age: toAge(profile.preferred_age.max_age) }
                };
                console.log(newProfile);
                return newProfile;
            });

    updateProfile = data => {
        let newData = {
            ...data,
            birth_date: toSeconds(data.birth_date),
            preferred_age: { min_age: toSeconds(data.preferred_age.min_age), max_age: toSeconds(data.preferred_age.max_age) }
        };

        return fetch(`${this.host}/users/update_profile`, {
            method: 'POST',
            headers: this.headers,
            body: JSON.stringify({ profile: newData })
        }).then(rep => rep.json());
    };

    updateFriends = friendsArray =>
        fetch(`${this.host}/users/update_friends`, {
            method: 'POST',
            headers: this.headers,
            body: JSON.stringify(friendsArray)
        }).then(rep => rep.json());

    req = (uri, data) =>
        fetch(`${this.host}/${uri}`, {
            method: 'POST',
            headers: this.headers,
            body: JSON.stringify(data)
        });
}

export default API;
