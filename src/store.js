import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from 'store/reducers';

function saveToLocalStorage(state) {
    try {
        const serializeData = JSON.stringify(state);
        localStorage.setItem('shipper_mini_app', serializeData);
    } catch (error) {
        console.log(error);
    }
}

function loadSerializedData() {
    try {
        const serializeData = localStorage.getItem('shipper_mini_app');
        if (!serializeData) return undefined;
        return JSON.parse(serializeData);
    } catch (error) {
        console.log(error);
        return undefined;
    }
}

export const configureStore = () => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
    return store;
    // const persistedState = loadSerializedData();
    // store.subscribe(() => saveToLocalStorage(store.getState()));
};
