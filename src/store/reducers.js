import { combineReducers } from 'redux';
import { routerReducer } from './router/reducers';
import { vkuiReducer } from './vk/reducers';
import { userInfoReducer } from './userInfo/reducers';
import { serverDataReducer } from './serverData/reducers';

export default combineReducers({
    vkui: vkuiReducer,
    router: routerReducer,
    userInfo: userInfoReducer,
    serverData: serverDataReducer
});
