import { SET_JWT_TOKEN, SET_REGISTERED, SET_INITIAL_DATA, SET_FRIENDS_LOADING, SET_PRIVATE_PROFILE } from './actionTypes';

export const setJWTToken = token => ({
    type: SET_JWT_TOKEN,
    payload: token
});

export const setRegistered = value => ({
    type: SET_REGISTERED,
    payload: value
});

export const setInitialData = data => ({
    type: SET_INITIAL_DATA,
    payload: data
});

export const setFriendsLoading = value => ({
    type: SET_FRIENDS_LOADING,
    payload: value
});

export const setPrivateProfile = value => ({
    type: SET_PRIVATE_PROFILE,
    payload: value
});
