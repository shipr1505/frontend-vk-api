import { SET_JWT_TOKEN, SET_REGISTERED, SET_INITIAL_DATA, SET_FRIENDS_LOADING, SET_PRIVATE_PROFILE } from './actionTypes';

const initialState = {
    registered: false,
    jwtToken: null,
    VKStorageLoaded: false,
    friendsLoading: false,
    privateAccount: false
};

export const serverDataReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case SET_JWT_TOKEN: {
            return {
                ...state,
                jwtToken: payload
            };
        }

        case SET_REGISTERED: {
            return {
                ...state,
                registered: true
            };
        }

        case SET_INITIAL_DATA: {
            return {
                ...state,
                VKStorageLoaded: true,
                jwtToken: payload.jwtToken,
                registered: payload.registered || false
            };
        }

        case SET_FRIENDS_LOADING: {
            return { ...state, friendsLoading: payload };
        }

        case SET_PRIVATE_PROFILE: {
            return { ...state, privateAccount: payload };
        }

        default: {
            return state;
        }
    }
};
