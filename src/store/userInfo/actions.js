import { SET_USER_INFO, SET_PROFILE_DATA } from './actionTypes';

export const setUserInfo = userInfo => ({
    type: SET_USER_INFO,
    payload: userInfo
});

export const setProfileData = profileData => ({
    type: SET_PROFILE_DATA,
    payload: profileData
});
