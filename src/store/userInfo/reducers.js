import { SET_USER_INFO, SET_PROFILE_DATA } from './actionTypes';

const initialState = {
    userInfo: null,
    profileData: null
};

export const userInfoReducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_USER_INFO:
            return { ...state, userInfo: payload };
        case SET_PROFILE_DATA:
            return { ...state, profileData: payload };

        default:
            return { ...state };
    }
};
