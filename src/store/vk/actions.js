import { SET_COLOR_SCHEME, SET_ACCESS_TOKEN, SET_QUERY_DATA, SET_EPIC_SHOW } from './actionTypes';

export const setEpicShow = value => ({
    type: SET_EPIC_SHOW,
    payload: value
});

export const setColorScheme = scheme => ({
    type: SET_COLOR_SCHEME,
    payload: scheme
});

export const setAccessToken = accessToken => ({
    type: SET_ACCESS_TOKEN,
    payload: accessToken
});

export const setQueryData = queryData => ({
    type: SET_QUERY_DATA,
    payload: queryData
});
