import { SET_ACCESS_TOKEN, SET_COLOR_SCHEME, SET_QUERY_DATA, SET_EPIC_SHOW } from './actionTypes';

const initialState = {
    accessToken: undefined,
    friendsPermission: false,
    queryData: null,
    colorScheme: 'client_light',
    showEpic: false
};

export const vkuiReducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_COLOR_SCHEME:
            return { ...state, colorScheme: payload };

        case SET_ACCESS_TOKEN: {
            return {
                ...state,
                accessToken: payload,
                friendsPermission: true
            };
        }

        case SET_QUERY_DATA:
            return { ...state, queryData: payload };

        case SET_EPIC_SHOW:
            return { ...state, epicShow: payload };

        default: {
            return state;
        }
    }
};
